# 虚拟机安装ubuntu及设置
## VMware安装ubuntu
1. 80G硬盘
2. 新建虚拟机时，选择`安装程序光盘映像文件`会使用简易安装给你。选择`稍后安装操作系统`，会有选择语言、键盘等。
3. 将虚拟机存储为单个文件
4. 断网安装
5. 使用easy install安装后，会自动加载一个`autoinst.flp`软盘镜像
6. 安装完后可以将所有CD/DVD、floppy移除
## 安装vmware tools
1. vmware安装目录下，有`linux.iso`文件
2. 虚拟机设置中添加CD/DVD文件(可能需要重启vmware player软件才会有CD/DVD)
## gnome桌面放大
`gsettings set org.gnoem.desktop.interface scaling-factor 2`
## ubuntu换源
```shell
sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak
vim /etc/apt/sources/list
```
或者在`Software & Updates`直接修改源  
[阿里的源](https://developer.aliyun.com/mirror/ubuntu?spm=a2c6h.13651102.0.0.3e221b11M1ZFxp)
## xftp连接
`ubuntu`初始安装，默认`ssh`只安装客户端(即只能`ssh`连接其他主机)，没有安装服务端(即不能被其他主机`ssh`连接主机)
```shell
# 安装`ssh`服务端
sudo apt install openssh-server
# 显示`sshd`即连接成功
ps -e | grep ssh
# 如果不显示
sudo /etc/init.d/ssh start
```
## 更新软件源
```shell
sudo apt update
sudo apt upgrade
```
# 准备阶段(linux)
安装`git`、`curl`、`wget`、`vim`、`docker`、`docker-compose`
```shell
sudo apt intall *
```
docker授权普通用户
```
```
docker切换镜像源
```shell
vi /etc/docker/daemon.json
# {
#   "registry-mirrors": ["https://ixmwku44.mirror.aliyuncs.com"]
# }
sudo systemctl daemon-reload
sudo systemctl restart docker
```
安装golang
```shell
wget https://golang.google.cn/dl/go1.18.linux-amd64.tar.gz
tar -zxvf https://golang.google.cn/dl/go1.18.linux-amd64.tar.gz
```
设置go环境变量
```shell
export PATH=$PATH:/home/jrc/workspaces/go/bin

export GOPATH=/home/jrc/workspaces/gopath
export PATH=$PATH:$GOPATH/bin/
```
# 启动网路
```shell
cd fabric-samples/test-network
./network.sh down
./network.sh up createChannel
```
# 设置Logspout
```shell
./monitordocker.sh fabric_test
```
# 打包智能合约（go）
go包将被安装在`vendor`文件夹
```shell
# 更改go代理
go env -w GOPROXY=https://goproxy.io,direct
go env -w GO111MODULE=on
go mod vendor
# 在链码目录下执行
GO111MODULE=on go mod vendor
```
配置环境变量,并将`FABRIC_CFG_PATH`设置为指向`fabric-samples`目录下的`core.yaml`文件
```shell
# 确保在test-network目录下
export PATH=${PWD}/../bin:$PATH
export FABRIC_CFG_PATH=$PWD/../config/
peer version
```
创建链码包
```shell
# 创建名为fabcar.tar.gz的包
# --lang 指定语言 --path 提供智能合约代码的位置 --label 指定链码标签
peer lifecycle chaincode package fabcar.tar.gz --path ../fabcar/go/ --lang golang --label fabcar_1.0
```
# 安装链码包
在Org1的peer上安装链码，设置环境变量来操作`peer`CLI
```shell
export CORE_PEER_TLS_ENABLED=true

export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
export CORE_PEER_ADDRESS=localhost:7051
```
在`peer`上安装链码
```shell
peer lifecycle chaincode install fabcar.tar.gz
```
在Org2上安装链码，设置环境变量
```shell
export CORE_PEER_LOCALMSPID="Org2MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt
export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp
export CORE_PEER_ADDRESS=localhost:9051
```
安装链码
```shell
peer lifecycle chaincode install fabcar.tar.gz
```
# 批准链码定义
查询链码包ID
```shell
peer lifecycle chaincode queryinstalled
```
链码ID带入环境变量
```shell
# 链码在组织级别被批准，命令只需要针对一个peer,通过gossip分发到其它peer
export CC_PACKAGE_ID=fabcar_1.0:dcc133ce1de415199463710a87548fa9556ebc91167a1a15391f395fc0da3d93
```
Org2批准链码(以默认背书政策)
```shell
# sequence 跟踪链码被定义和更新的次数
peer lifecycle chaincode approveformyorg -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --channelID mychannel --name fabcar --version 1.0 --package-id $CC_PACKAGE_ID --sequence 1 --tls --cafile "${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem"
```
Org1批准链码
```shell
export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
export CORE_PEER_ADDRESS=localhost:7051

peer lifecycle chaincode approveformyorg -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --channelID mychannel --name fabcar --version 1.0 --package-id $CC_PACKAGE_ID --sequence 1 --tls --cafile "${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem"
```
# 将链码定义提交给通道
检查通道成员是否批准了相同的链码定义
```shell
peer lifecycle chaincode checkcommitreadiness --channelID mychannel --name fabcar --version 1.0 --sequence 1 --tls --cafile "${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem" --output json
```
提交链码定义到通道
```shell
# --peerAddresses 定位Org1和Org2
peer lifecycle chaincode commit -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --channelID mychannel --name fabcar --version 1.0 --sequence 1 --tls --cafile "${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem" --peerAddresses localhost:7051 --tlsRootCertFiles "${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt" --peerAddresses localhost:9051 --tlsRootCertFiles "${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt"
```
确认链代码定义已提交到通道
```shell
peer lifecycle chaincode querycommitted --channelID mychannel --name fabcar --cafile "${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem"
```
# 调用链码
调用初始InitLedger函数
```shell
peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls --cafile "${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem" -C mychannel -n fabcar --peerAddresses localhost:7051 --tlsRootCertFiles "${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt" --peerAddresses localhost:9051 --tlsRootCertFiles "${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt" -c '{"function":"InitLedger","Args":[]}'
```
使用query函数
```shell
peer chaincode query -C mychannel -n fabcar -c '{"Args":["QueryAllCars"]}'
```

